![logo](http://i.imgur.com/MUEExsT.png)

# API do Cardápio do RU (CRU)

<hr>

A API "CRU" (Cardápio do Restaurante Universitário) foi criada com a intenção de fornecer informações sobre as refeições servidas nos Restaurante Universitários da Universidade Federal do Pará, criando um histórico informativo que possa auxiliar em pesquisas ou mesmo no uso do próprio frequentador dos RUs.

Todas as informações são retiradas do site oficial do Restaurante Universitário através de um _scraper_ que faz checagens periódicas para avaliar mudanças no cardápio e atualizar o banco de dados da API.

Nenhum lucro financeiro foi ou será ganho com o desenvolvimento e uso da API. Nunca. Jamás. Never. Mai. Numquam. Nigdy. никогда.

[Thassio Victor](http://twitter.com/thassiov), o programador.

<hr>

## Índice

- [O que esperar da API](#o-que-esperar-da-api)
- [Tecnologias usadas](#tecnologias-usadas)
- [Rotas](#rotas)
- [Estrutura dos dados](#estrutura-dos-dados)
- [Exemplos de uso](#exemplos-de-uso)

<hr>

## O que esperar da API

O banco de dados do sistema é atualizado a cada 30 minutos. Portanto, caso haja alguma mudança no conteúdo do site ru.ufpa.br, as alterações devem entrar em vigor dentro dessa janela de tempo.

As informações que a API pode oferecer são limitadas, logicamente, ao conteúdo postado no site oficial do RU da UFPA. Portanto, erros de digitação, simplificações/abreviações de palavras, entre outros comportamentos indesejados, são previstos já que a atualização das informações no site do RU é feita por humanos. Entretanto, parte desses comportamentos indesejados já são corrigidos automaticamente durante a etapa de scrape e parsing do site citado.

## Tecnologias usadas

O sistema foi desenvolvido em JavaScript, no ambiente [Node.js](http://nodejs.org/), usando o framework [Express](http://expressjs.com) como backend e [MongoDB](https://www.mongodb.org) como banco de dados.

Outros softwares são usados no projeto e todos eles estão listados em [package.json](package.json).

## Rotas

- [GET /api/v1/cardapio/atual](#/api/v1/cardapio/atual)
- [GET /api/v1/cardapio/dia/:dataDoCardapio](#/api/v1/cardapio/dia/:dataDoCardapio)
- [GET /api/v1/cardapio/:id](#/api/v1/cardapio/:id)
- [GET /api/v1/cardapios/lista/](#/api/v1/cardapios/lista/)
- [GET /api/v1/comida/:id](#/api/v1/comida/:id)
- [GET /api/v1/comidas/lista/](#/api/v1/comidas/lista/)

### /api/v1/comidas/lista/

### /api/v1/comida/<id>

### /api/v1/cardapio/dia/<data-do-cardapio>

### /api/v1/cardapios/lista/

### /api/v1/cardapio/<id>

### /api/v1/cardapio/atual

## Estrutura dos dados

## Exemplos de uso

## Licença

[MIT](LICENSE)

