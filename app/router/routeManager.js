/**
 * routeManager.js
 * Gerencia as rotas da API.
 * 
 * @author Thassio Victor <tvmcarvalho@gmail.com>
 */

//@TODO: transformar , de alguma forma, isso tudo em promise

var docsRoute = require('./docs/docsRoute');
var cardapiosAtual = require('./cardapios/atual');

module.exports = function (app) {

	/**
	 * Rota que leva até thassiov.github.io/cru onde está a documentação 
	 * do projeto
	 */
	app.get('/', function(req, res) {
		docsRoute(req, res);
	});

	/**
	 * Retorna o cardápio mais atual do RU da UFPA
	 */
	app.get('/api/v1/cardapios/atual', function(req, res) {
		cardapiosAtual(req, res);
	});

};
