/**
 * router/docs/docsRoute.js
 * Rota que leva até thassiov.github.io/cru onde está a documentação
 * do projeto.
 * 
 * @author Thassio Victor <tvmcarvalho@gmail.com>
 */

module.exports = function (req, res) {
	//@TODO: mudar para 'cru.js.org' quando o domínio for aceito por js.org
	res.redirect('http://thassiov.github.io/cru');
};