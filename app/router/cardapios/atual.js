/**
 * router/cardapios/atual.js
 * Retorna o cardápio mais atual provido por ru.ufpa.br
 * @author Thassio Victor <tvmcarvalho@gmail.com>
 */

var Q = require('q');
var cardapioSemana = require('../../models/c_cardapio_semana');
var cardapioDia = require('../../models/c_cardapio_dia');
var comida = require('../../models/c_comida');

module.exports = function  (req, res) {
	
	/**
	 * Vamos explicar a variável cardapioSemanaStorage...
	 * 
	 * Meu dilema com esse módulo (router/carcapios/atual.js) era que, na hora 
	 * de popular os cardápios do dia (cardapioDia) com as comidas (comida), a 
	 * promise chain só me retornava um array com os cardapioDia de cada dia da 
	 * semana, mas não o objeto completo (comida dentro de cardapioDia dentro de 
	 * cardapioSemana).
	 *
	 * Li um artigo sobre promises e vi que tudo o que ele falava sobre boas 
	 * práticas e sobre como fazer uma boa cadeia de promises eu já estava 
	 * fazendo (organicamente. Nem sabia que tava fazendo certo, mas tava fazendo).
	 * Mas ele falou (quase) exatamente do problema que eu tava tendo: como 
	 * retornar dois objetos vindos de duas funções diferentes para uma única 
	 * função. A primeira solução que ele dava, e a que eu implementei com uma 
	 * certa incredulidade, foi a de criar uma variável fora do escopo da cadeia 
	 * para receber o valor retornado pela primeira função. Depois, quando a 
	 * segunda função retornasse o valor para a terceira, a qual receberia os 
	 * dois valores, de dentro do escopo da terceira função eu poderia acessar 
	 * a 'variável global' que havia criado para armazenar o valor da primeira 
	 * função (faz sentido? Pra mim fez).
	 *
	 * Só que tinha um problema: por algum motivo bizarro, quando eu tentava 
	 * algo parecido, o primeiro valor ia incompleto (?). Era um objeto com, 
	 * por exemplo, 5 propriedades, mas só conseguia visualizar 3. Eu não sei 
	 * que diabos aconteceu aqui, mas dessa vez funcionou e não vou mexer nessa 
	 * variável do demônio.
	 *
	 * É isso.
	 */
	//@TODO: descobrir porque esse diabo dessa variável satisfaz o problema
	var cardapioSemanaStorage;

	cardapioSemana.findOneQ({},{}, {sort: {'created_at':-1}}).
		then(function populateCardapioSemana (value) {
			cardapioSemanaStorage = value;
			return cardapioSemana.populateQ(value,{path:'cardapios'});
		}).
		then(function populateCardapioDia (value) {
			return Q.all(value.cardapios.map(function showComida (obj) {
				return cardapioDia.populateQ(obj, {path:'almoco jantar'});
				})
			);
		}).
		then(function sendPopulatedCardapioSemana(value) {
			res.send(cardapioSemanaStorage);
		}).
		fail(function failedForwarding (reason) {
			//@TODO: Criar um retorno melhor (para os logs e para o usuário)
			console.log(reason);
		});
};

