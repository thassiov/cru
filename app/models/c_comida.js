var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * Modelo que armazena informações sobre as comidas no cardápio
 * @property {String} nome Nome da comida
 * @property {String} registrado_em Data de registro do documento
 * @property {String} categoria Categoria em que a comida pertence
 * @type {Schema}
 */
var comida = new Schema({
	nome : String,
	registrado_em : String,
	categoria : String
}, {collection: 'comida'});

module.exports = mongoose.model('comida', comida);