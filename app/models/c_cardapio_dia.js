var mongoose = require('mongoose-q')();
var Schema = mongoose.Schema;

/**
 * Modelo que armazena informações sobre o cardápio do dia
 * @property {String} registrado_em Data de registro do objeto
 * @property {String} dia_da_semana Dia da semana, na forma <dia>-feira 
 * em que o cardápio estrá disponível
 * @property {String} data_do_cardapio Data em que o cardápio estará disponível
 * @property {Array} almoco Array de ObjectIds que apontam para a coleção 'comida'
 * @property {Array} jantar Array de ObjectIds que apontam para a coleção 'comida'
 * @type {Schema}
 * @author Thassio Victor <tvmcarvalho@gmail.com>
 */
var cardapio_dia = new Schema({
	registrado_em : String,
	dia_da_semana : String,
	data_do_cardapio : String,
	almoco : [{type: Schema.Types.ObjectId, ref: 'comida'}],
	jantar : [{type: Schema.Types.ObjectId, ref: 'comida'}] 
}, {collection: 'cardapio_dia'});

module.exports = mongoose.model('cardapio_dia', cardapio_dia);
