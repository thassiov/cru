
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * Modelo que armazena informações sobre a execução do módulo scrapeNparse
 * @property {String} hora_inicio Registra o timestamp do início da execução
 * @property {String} hora_termino Registra o timestamp do término da execução
 * @property {String} arquivo_gerado Registra a path para o documento HTML 
 *                                   baixado de ru.ufpa.br
 * @property {String} cardapio_gerado ObjectId, convertido em string, do 
 *                                    documento gerado contendo o cardápio
 *                                    da semana.
 * @type {Schema}
 * 
 */
var execucao = new Schema({
	hora_inicio : String,
	hora_termino : String,
	arquivo_gerado : String,
	cardapio_gerado : String,
}, {collection: 'execucao'});

module.exports = mongoose.model('execucao', execucao);