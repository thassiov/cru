var mongoose = require('mongoose-q')();
var Schema = mongoose.Schema;

/**
 * Modelo que armazena informações sobre os cardápios da semana
 * @property {String} registrado_em Data de registro do objeto
 * @property {Array} vigencia_do_cardapio Array que possue dois objetos do
 *  tipo Date onde o primeiro indica o início da vigência do cardápio e o 
 *  segungo o término da vigẽncia.
 * @property {Array} cardapios Array de ObjectIds que apontam para a coleção 
 * 'cardapio_dia'.
 * @type {Schema}
 * @author Thassio Victor <tvmcarvalho@gmail.com>
 */
var cardapio_semana = new Schema({
	registrado_em: String,
	vigencia_do_cardapio: Array,
	cardapios: [{type: Schema.Types.ObjectId, ref: 'cardapio_dia'}]
}, {collection: 'cardapio_semana'});

module.exports = mongoose.model('cardapio_semana', cardapio_semana);