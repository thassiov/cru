var db = require('mongoose');
var dbManager = require('../config/scrapeNparse/dbManager');
var raiz = require('../config/scrapeNparse/raiz');
var cardapio_semana = require(raiz() + "/app/models/c_cardapio_semana");
var cardapio_dia = require(raiz() + "/app/models/c_cardapio_dia");
var comida = require(raiz() + "/app/models/c_comida");

module.exports = function(app){

	/**
	 * Redirecionamento padrão
	 */
	app.get('/', function(req, res) {
		res.redirect('/api');
	});

	/**
	 * Rota para o manual da API
	 * @author Thassio Victor <tvmcarvalho@gmail.com>
	 */
	app.get('/api/', function(req, res, next) {
		res.sendFile(__dirname + '/api-man-page/index.html');
	});

	/**
	 * Retorna um JSON com o cardápio mais atual provido por ru.ufpa.br
	 * @author Thassio Victor <tvmcarvalho@gmail.com>
	 */
	app.get('/api/v1/cardapio/atual', function(req, res, next) {
		dbManager.buscarUltimoDocumentoSalvo("cardapio_semana", function(err, doc) {
			if (err) {throw err;}
			cardapio_semana.populate(doc,{path:'cardapios'}, function(err, doc) {
				if (err) {throw err;}
				cardapio_dia.populate(doc.cardapios, {path:'almoco jantar'}, function(err, docs) {
					if (err) {throw err;}
					doc.cardapios = docs;
					res.json(doc);
				});
			});
		});
	});

	/**
	 * Retorna um JSON com o cardápio do dia baseado na data no formado dd-mm-aaaa
	 * @author Thassio Victor <tvmcarvalho@gmail.com>
	 */
	app.get('/api/v1/cardapio/dia/:dataDoCardapio', function(req, res, next) {
		var dataDoCardapio = req.params.dataDoCardapio;
		if (dataDoCardapio.search('-') > -1) {
			var ddmmaa = dataDoCardapio.split('-');
			if (ddmmaa.length == 3) {
				var dd = parseInt(ddmmaa[0]);
				var mm = parseInt(ddmmaa[1]) - 1;
				var aa = parseInt(ddmmaa[2]);
				var datePointer = String(new Date(aa, mm, dd).getTime());
				cardapio_dia.findOne({"data_do_cardapio_str":datePointer})
					.populate({path:'almoco jantar'}).exec(function(err, data){
						if (err) {res.json(err);};
						res.json(data);
				});
			}
		}
	});

	/**
	 * Retorna um JSON com o cardápio indicado pelo ObjectId na URL. O 
	 * objeto já vai populado
	 * @author Thassio Victor <tvmcarvalho@gmail.com>
	 */
	app.get('/api/v1/cardapio/:id', function(req, res, next) {
		cardapio_semana.find({_id: req.params.id})
		.populate('cardapios')
		.exec(function(err, docs) {
			var opt = {
				path: 'cardapios.almoco cardapios.jantar',
				model: 'comida'
			}
			if (err) return res.json(500);
			cardapio_semana.populate(docs, opt, function(err, docs) {
				res.json(docs);
			})
		});
	});

	/**
	 * Retorna um JSON com uma lista de todos os cardápios disponíveis no banco,
	 * mostrando apenas 'vigencia_do_cardapio' e 'vigencia_do_cardapio_str', além 
	 * do _id. Ele é ordenado do cardápio com a vigência mais antiga para a mais nova.
	 * @author Thassio Victor <tvmcarvalho@gmail.com>
	 */
	app.get('/api/v1/cardapios/lista/', function(req, res, next) {
		cardapio_semana.find({},'vigencia_do_cardapio vigencia_do_cardapio_str ',
			{sort: {'vigencia_do_cardapio_str[0]': 1}}, function(err, comidas) {
			if (err) {res.json(err)};
			res.json(comidas);
		});
	})

	/**
	 * Rota um JSON com um item da coleção "comida"
	 * @author Thassio Victor <tvmcarvalho@gmail.com>
	 */
	app.get('/api/v1/comida/:id', function(req, res, next) {
		comida.find({_id: req.params.id}).exec(function(err, doc) {
			if (err) {res.json(err)};
			res.json(doc);
		});
	});

	/**
	 * Rota que retorna um JSON com todos os elementos contidos na coleção 
	 * "comida" mostrando apenas 'nome' e '_id' e em ordem alfabética
	 * @author Thassio Victor <tvmcarvalho@gmail.com>
	 */
	app.get('/api/v1/comidas/lista/', function(req, res) {
		comida.find({},'nome',{sort: {'nome': 1}}, function(err, comidas) {
			if (err) {res.json(err)};
			res.json(comidas);
		});
	});
};