/*
	Se retornar true, o cardapio já existe e está salvo no banco de dados.
	Caso retorne false, significa que o cardápio precisa ser salvo ainda.

*/

var dbManager = require('./dbManager');

module.exports = function(cardapioInput, callback) {
	var cardapio_semana_length = 0;
	console.log("[INÍCIO] cardapio_semana_length: " + cardapio_semana_length);
	dbManager.buscarUltimoDocumentoSalvo("cardapio_semana", function(err, doc) {
		if (err) {callback(err);}
		if (doc !== null) {
			if (doc.vigencia == cardapioInput.vigencia) {
				cardapio_semana_length = doc.cardapios.length;
				console.log("[ATRIBUIÇÃO] cardapio_semana_length: " + cardapio_semana_length);
				doc.cardapios.forEach(function(elem) {
					dbManager.buscarUmPopulate("cardapio_dia", "_id", elem, ['almoco', 'jantar'], function(err, cdd) {
						if (err) {callback(err);}
						var pointer = indexOfObjectArray(cardapioInput.cardapio, cdd[0].data, "data");
						if (pointer !== -1
							&& cdd[0].data == cardapioInput.cardapio[pointer].data
							&& cdd[0].dia_da_semana == cardapioInput.cardapio[pointer].dia_da_semana
							&&  cardapioCheck(cdd[0].almoco, cardapioInput.cardapio[pointer].almoco) == true
							&&  cardapioCheck(cdd[0].jantar, cardapioInput.cardapio[pointer].jantar) == true) {
								cardapio_semana_length--;
								console.log("[PROCESSAMENTO] cardapio_semana_length: " + cardapio_semana_length);
								if (cardapio_semana_length == 0) {
									console.log("[FINAL] cardapio_semana_length: " + cardapio_semana_length);
									console.log("O cardápio já consta no banco de dados");
									callback(null, true);
								}
						} else {
							console.warn("Há um cardápio do dia que não está no cardápio da semana");
							console.log("[FINAL] cardapio_semana_length: " + cardapio_semana_length);
							callback(null, false);
						}
					});
				});
			} else {
				console.warn("A vigência mudou");
				console.log("[FINAL] cardapio_semana_length: " + cardapio_semana_length);
				callback(null, false);
			}
		} else {
			console.warn("Documento não encontrado no banco de dados.");
			console.log("[FINAL] cardapio_semana_length: " + cardapio_semana_length);
			callback(null, false);
			}
	});

	function cardapioCheck (refeicaoDB, refeicaoInput) {
		for (var i = 0; i < refeicaoDB.length; i++) {
			if (refeicaoDB[i].nome == refeicaoInput[i].nome) {
				if (refeicaoDB.length == i+1) {
					return true;
				}
				continue;
			} else {
				return false;
			}
		}
	}

	function indexOfObjectArray (object, termoDeBusca, propriedade) {
		for (var i = 0; i < object.length; i++) {
			if (object[i][propriedade] === termoDeBusca) {
				return i;
			}
		}
		return -1;
	}
}