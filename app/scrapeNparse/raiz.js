var fs = require('fs');

module.exports = function() {
	var rootDirSplit = __dirname.split("/");
	var rootDir = "";
	var loopCounter = 0;
	rootDirSplit.forEach(function(elem) {
		if (elem !== "") {
			if (loopCounter < rootDirSplit.length - 3) {
			rootDir +="/" + elem;
			}
		}
		loopCounter++;
	});
	return rootDir;
};