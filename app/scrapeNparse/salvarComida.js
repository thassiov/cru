var moment = require('moment-timezone');
var dbManager = require('./dbManager');
var raiz = require('./raiz');
var Comida = require(raiz() + '/app/models/c_comida');

/**
 * Recebe um array com os nomes das comidas extraídas do cardápio por docParse.
 * Cada item do array é checado no banco de dados para não haver nenhuma 
 * duplicação para depois serem salvos.
 * @param  {array}   comidas2check Lista com nome das comidas retiradas do cardápio
 * @param  {Function} callback      Retorna um erro durante o armazenamento dos 
 * dados ou um array contendo objetos 'c_comida' devidamente populados.
 * @author Thassio Victor <tvmcarvalho@gmail.com>
 */
module.exports = function(comidas2check, callback){

	var ComidaArray = [];
	var comidaArrayResponse = [];
	dbManager.buscarMuitos("comida", "nome", comidas2check, function(err, docs) {
		if(err) {callback(err);}
		comidas2check.forEach(function(elem) {			
			var teste = indexOfObjectArray(docs, elem, "nome");
			if ( teste === -1) { // se não tiver entrada no DB, crie uma e adicione ao array
				var comida_obj = new Comida();
				comida_obj.nome = elem;
				comida_obj.registrado_em = moment().tz('America/Belem').format();
				comida_obj.categoria = "";
				ComidaArray.push(comida_obj);
			} else { // caso já tenha, adicione as informações no array
				comidaArrayResponse.push(docs[teste]);
			}
		});

		dbManager.salvarMuitos("comida", ComidaArray, function(err, docs) {
			if(err) {callback(err);}
			docs.forEach(function(elem) {
				comidaArrayResponse.push(elem);
			});
			callback(null, comidaArrayResponse);	
		});
	});

	/**
	 * Recriação do método 'indexOf' que funciona em array de objetos.
	 * @param  {array} objectArray   Array contendo os objetos a serem analisados.
	 * @param  {string} termoDeBusca Valor da propriedade a ser buscada.
	 * @param  {string} propriedade  Nome da propriedade a ser buscada.
	 * @return {number}              Retorna a posição do objeto no array.
	 * @author Thassio Victor <tvmcarvalho@gmail.com>
	 */
	function indexOfObjectArray (objectArray, termoDeBusca, propriedade) {
		for (var i = 0; i < objectArray.length; i++) {
			if (objectArray[i][propriedade] === termoDeBusca) {
				return i;
			}
		}
		return -1;
	}
};