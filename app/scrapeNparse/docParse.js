var moment = require('moment-timezone');
var jsdom = require('jsdom');
var raiz = require('./raiz');
var $ = "";

/**
 * Faz o parsing do documento HTML baixado por docDownload. É responsável por 
 * extrair todas as informações importantes que dizem respeito ao cardápio da 
 * semana.
 * @param  {string}   file     Path do último arquivo baixado por docDownload
 * @param  {Function} callback Retorna dois objetos (se existirem): 'cardapio', 
 * que é análogo ao modelo 'c_cardapio_semana', e a lista de comidas retiradas 
 * do cardápio em 'comidas2check'.
 * @author Thassio Victor <tvmcarvalho@gmail.com>
 */
module.exports = function(file, callback) {

	/**
	 * Lista de comidas retirada do documento HTML. Esta variável é enviada por
	 *  callback.
	 * @type {Array}
	 * @author Thassio Victor <tvmcarvalho@gmail.com>
	 */
	var comidas2check = [];

	/**
	 * Objeto semelhante ao modelo 'c_cardapio_semana' que é utilizado para 
	 * armazenar os dados retirados do documento HTML.
	 * @type {Object}
	 * @author Thassio Victor <tvmcarvalho@gmail.com>
	 */
	var tmpCardapio = {};
		
	console.log('Processando');
	/*http://code.jquery.com/jquery.js*/
	jsdom.env(file, [ raiz() + '/public/assets/js/jquery.js'], function(err, window) {
		if (err) {callback(err);}

		$ = window.$;
		var cardapio = getCardapio();
		callback(null, cardapio, comidas2check);
	});

	/**
	 * Extrai as informações do documento HTML baixado. Essa foi uma das partes 
	 * do projeto inteiro que me deu mais trabalho e consumiu tempo.
	 * @return {object} tmpCardapio
	 * @author Thassio Victor <tvmcarvalho@gmail.com>
	 */
	function getCardapio() {

		var infoData = getInfoData();

		var diaInicial = infoData.diaInicial;
    var mesInicial = infoData.mesInicial;
    var anoInicial = infoData.anoInicial;
    var diaFinal = infoData.diaFinal;
    var mesFinal = infoData.mesFinal;
    var anoFinal = infoData.anoFinal;

		tmpCardapio.nome = "Cardapio do RU da UFPA";
		tmpCardapio.registrado_em = moment().tz('America/Belem').format();
		tmpCardapio.vigencia_do_cardapio = [];
		
		// tmpCardapio.vigencia_do_cardapio.push(new Date(anoInicial, 
		// 	mesInicial, diaInicial));

		tmpCardapio.vigencia_do_cardapio.
			push(moment(new Date(anoInicial, mesInicial, diaInicial)).
			tz('America/Belem').
			format());

		tmpCardapio.vigencia_do_cardapio.
			push(moment(new Date(anoFinal, mesFinal, diaFinal)).
			tz('America/Belem').
			format());

		// tmpCardapio.vigencia_do_cardapio.push(new Date(anoFinal, 
		// 	mesFinal, diaFinal));

		tmpCardapio.cardapio = [];

		var big = $(".contentpaneopen table tbody")[0].childNodes;
		// big == NodeList [ #text, <tr>, #text, ...]
		var counter = 0;
		$(big).each(function() {		
			$(this).each(function() {
				if (this.nodeType != 3) {
					// this == <tr...>, <tr...>, <tr...> ...
					// PRIMEIRA LINHA = DIA | ALMOÇO | JANTAR
					if (this == this.parentNode.childNodes[1]) {
						var row1 = this;
						var td1row1 = row1.childNodes[1].textContent.trim();
						var td2row1 = row1.childNodes[3].textContent.trim();
						var td3row1 = row1.childNodes[5].textContent.trim();
					} else {
						// DEMAIS LINHAS

						var row = this;
						// td1row = DIAS DA SEMANA
						var td1row = convertElement2Value(getTextNodesIn(row.childNodes[1], false));
						
						// td2row = ALMOÇO
						var td2row = convertElement2Value(getTextNodesIn(row.childNodes[3], false));
						
						// td3row = JANTA
						var td3row = convertElement2Value(getTextNodesIn(row.childNodes[5], false));

						var dia = parseDia(td1row[0]);
						var ar_almoco = parseRango(td2row);
						var ar_jantar = parseRango(td3row);
						
						/**
						 * !!NÃO LEMBRO DIREITO O QUE ESSA CONDIÇÃO FAZ!!
						 * Esse 'if' foca (de alguma forma) no problema do javascript de 
						 * tratar os meses como um array [0-11] ou algo assim.
						 */
						var data_do_cardapio_tmp;
						var diasNoMes = moment(anoInicial + '-' + mesInicial, 'YYYY-MM').daysInMonth();
						if (diaInicial > diasNoMes) {
							if (mesInicial == 12) {

								// data_do_cardapio_tmp = new Date(
								// anoInicial + 1, 1, 1);//happy new year!

								data_do_cardapio_tmp = moment(new Date(
								anoInicial + 1, 1, 1)).
								tz('America/Belem').
								format();//happy new year!

							} else {

								// data_do_cardapio_tmp = new Date(
								// 	anoInicial, mesInicial + 1, 1);
								
								data_do_cardapio_tmp = moment(new Date(
								anoInicial, mesInicial, 1)).
								tz('America/Belem').
								format();

							}
						} else {

							// data_do_cardapio_tmp = new Date(anoInicial, 
							// 	mesInicial , diaInicial + counter);

								data_do_cardapio_tmp = moment(new Date(
								anoInicial, mesInicial, diaInicial + counter)).
								tz('America/Belem').
								format();
						}

						// Objeto analogo a c_cardapio_dia
						var tmp_obj = {
							registrado_em: moment().tz('America/Belem').format(),
							dia_da_semana: dia,
							data_do_cardapio : data_do_cardapio_tmp,
							almoco: ar_almoco,
							jantar: ar_jantar
						};

						tmpCardapio.cardapio.push(tmp_obj);
						counter++;
					}
				}
			});
		});	
		return tmpCardapio;
	}

	/**
	 * Extrai o valor de um elemento HTML. O uso dele se faz muito necessário 
	 * por conta da (insana) quantidade de elementos aninhados na página do 
	 * cardápio do RU. O que ele faz (na teoria) é copiar os textNodes que estão 
	 * dentro de um dado elemento e os retorna dentro de um array. Na prática ele 
	 * expôe o elemento para que a função 'convertElement2Value' extraia o valor.
	 * @param  {object} node                   Elemento retirado do documento HTML
	 * @param  {boolean} includeWhitespaceNodes Flag que indica se podem ser 
	 * retornados dentro do array textNodes em branco (caso sejam encontrados).
	 * @return {array} textNodes                Array contendo os textNodes
	 * encontrados no elemento HTML 'node'
	 *
	 * Retirado de uma thread no Stack Overflow
	 */
	function getTextNodesIn(node, includeWhitespaceNodes) {
	  var textNodes = [], nonWhitespaceMatcher = /\S/;

	  function getTextNodes(node) {
	      if (node.nodeType == 3) {
	          if (includeWhitespaceNodes || nonWhitespaceMatcher.test(node.nodeValue)) {
	              textNodes.push(node);
	          }
	      } else {
	          for (var i = 0, len = node.childNodes.length; i < len; ++i) {
	              getTextNodes(node.childNodes[i]);
	          }
	      }
	  }

	  getTextNodes(node);
	  return textNodes;
		}

	/**
	 * Função auxiliar de getTextNodesIn. As duas trabalham em conjunto
	 * @param  {[type]} elemArray Array de elementos HTML os quais serão
	 * extraídos os valores.
	 * @return {array}           Nomes extraídos
	 * @author Thassio Victor <tvmcarvalho@gmail.com>
	 */
	function convertElement2Value(elemArray){
		var arrayResponse = [];
			elemArray.forEach(function(elemValue) {
				arrayResponse.push(elemValue.__nodeValue);
			});
			return arrayResponse;
	}

	/**
	   * Extrai as informações sobre a vigência do cardápio.
	   * @return {object} result Objeto contendo dias de vigência do cardápio,
	   *  mês e ano.
	   * @author Thassio Victor <tvmcarvalho@gmail.com>
	   */
 	function getInfoData() {
		var result = {};
		var tdTarget = $(".contentpaneopen tbody tr td")[0].childNodes;
		$(tdTarget).each(function() {
		  if (this.nodeType != 3) {
		    if (this.textContent !== "" && this.tagName !== "TABLE") {
		      var splitCardapio = this.textContent.split(" ");
		      if ((splitCardapio[0].toLowerCase() == "cardápio" ||
		           splitCardapio[0].toLowerCase() == "cardapio") &&
		           splitCardapio[splitCardapio.length - 1].search("/") != -1) { // melhorar regexp

		        var diaInicial, mesInicial, anoInicial;
		        var diaFinal, mesFinal, anoFinal;
		        
		        diaFinal = splitCardapio[splitCardapio.length - 1].trim();
		        var dataFinalSplit = diaFinal.split("/");
		        diaFinal = dataFinalSplit[0];
		        mesFinal = dataFinalSplit[1];
		        if (dataFinalSplit[2].length == 2) {
		            anoFinal = '20' + dataFinalSplit[2];
		          } else if(dataFinalSplit[2].length == 4){
		              anoFinal = dataFinalSplit[2];
		            }

		        /**
		         * Busca pela primeira ocorrência de um número no array, que pode 
		         * ser uma data no formato 'dd', ou de uma String que possua "/", 
		         * no caso uma data no formado 'dd/mm' ou 'dd/mm/aaaa'
		         */
		        for (var i = 0 ; i <= splitCardapio.length - 1; i++) {
		          if ((parseInt(splitCardapio[i]) > 0 && 
		                parseInt(splitCardapio[i]) <= 31) || 
		                splitCardapio[i].search("/") !=-1) {
		          			var tmpSplit;
			              switch(splitCardapio[i].split("/").length){
			                case 1:
			                	tmpSplit = splitCardapio[i].split("/");
			                  diaInicial = tmpSplit[0];

			                  /**
			                   * Testa se o cardápio está em um período 'entre meses' 
			                   * para poder incluir um mês e ano iniciais.
			                   * O período entre meses se configura tendo o 'diaInicial' 
			                   * como sendo maior ou igual a 25, o que dá aproximadamente 
			                   * 1 semana para o início do novo mês, e o 'diaFinal' sendo 
			                   * menor ou igual a 7, dando 1 semana de diferença para o 
			                   * mês anterior.
			                   */
			                  if (Number(diaInicial)>=25 &&	Number(diaFinal)<=7) {
			                  	if (Number(mesFinal) == 1) {
			                  		mesInicial = '12';
			                  		anoInicial = String(Number(anoFinal) - 1);
			                  	} else {
			                  		mesInicial = String(Number(mesFinal) - 1);
			                  		anoInicial = anoFinal;
			                  	}
			                  }
			                  break;

			                case 2:
			                  tmpSplit = splitCardapio[i].split("/");
			                  diaInicial = tmpSplit[0];
			                  if (tmpSplit[1] !== "") {
			                    mesInicial = tmpSplit[1];  
			                  } else {
			                    mesInicial = mesFinal;
			                  }
			                  anoInicial = anoFinal;
			                  break;

			                case 3:
			                  tmpSplit = splitCardapio[i].split("/");
			                  diaInicial = tmpSplit[0];
			                  mesInicial = tmpSplit[1];
			                  if (tmpSplit[2].length == 2) {
			                    anoInicial = '20' + tmpSplit[2];
			                  } else if(tmpSplit[2].length == 4){
			                    anoInicial = tmpSplit[2];
			                  } else {
			                    anoInicial = anoFinal; // sei que ta errado, mas por enquando vai ficar assim
			                  }
			                  break;

			                default:
			                  console.log("wtf");
			                  break;
			              }
		              break; // 'for(;;)'
		          }
		        }
		        result.diaInicial = Number(diaInicial);
		        result.mesInicial = Number(mesInicial) - 1;
		        result.anoInicial = Number(anoInicial);
		        result.diaFinal = Number(diaFinal);
		        result.mesFinal = Number(mesFinal) - 1;
		        result.anoFinal = Number(anoFinal);
		      }
		    }
		  }   
		});
		return result;
 	}

	/**
	 * Ajusta a string da refeição retirando os espaços em branco, 
	 * trocando os "c/" por "com" e mudando a capitalização das palavras.
	 * Nessa função o array 'comidas2check' é populado com os nomes das comidas
	 *  que serão enviados via callback.
	 * @param  {array} rango Lista com os nomes das comidas que serão processadas
	 * @return {array} resultRango Lista com os nomes das comidas ajustados
	 * @author Thassio Victor <tvmcarvalho@gmail.com>
	 */
	function parseRango(rango) {
		var resultRango = [];
		rango.forEach(function(elem) {
			elem = elem.trim();
			if (elem !== "" && elem !== "-") {
				if (elem.substr(0,2) == "- ") {
					elem = elem.substr(2, elem.length -1);
				}
				var trimmed = elem.trim().replace("C/", "COM").toLowerCase();
				trimmed = trimmed.charAt(0).toUpperCase() + trimmed.substring(1);
				var toPush = {nome: trimmed, ID: ""};
				resultRango.push(toPush);

				if (comidas2check.indexOf(trimmed) == -1) {
					comidas2check.push(trimmed);
				}
			}
		});
		return resultRango;	
	}

	/**
	 * Extrai a data do cardápio do dia.
	 * @param  {object} dia Elemento HTML onde está escrito o dia do cardápio.
	 * @return {string}     Dia do cardapio
	 */
	function parseDia(dia) {
		var parsed = dia.toLowerCase();
		parsed = parsed.charAt(0).toUpperCase() + parsed.substring(1);
		return parsed;
	}

};