var mongoose = require('mongoose');
// Essa conexão deveria estar em server.js
//mongoose.connect('mongodb://cardapiodoru:cardapiodoru@localhost/cardapiodoru');
// DB local
//mongoose.connect('mongodb://cardapiodoru:cardapiodoru@localhost/cardapiodoru');
// DB remoto
//mongoose.connect('mongodb://cardapiodoru:cardapiodoru@ds063240.mongolab.com:63240/cardapiodoru');
var raiz = require('./raiz');

//modelos
var comida = require(raiz() + '/app/models/c_comida');
var cardapio_dia = require(raiz() + '/app/models/c_cardapio_dia');
var cardapio_semana = require(raiz() + '/app/models/c_cardapio_semana');
var execucao = require(raiz() + '/app/models/c_execucao');

var modelsArray = [];
modelsArray.push(comida);
modelsArray.push(cardapio_dia);
modelsArray.push(cardapio_semana);
modelsArray.push(execucao);

// POSSÍVEL CONDIÇÃO DE CORRIDA V

exports.salvarUm = function (construtor, documento, callback) {
	var obj = selectModel(construtor);
	var toSave = new modelsArray[obj](documento);
	toSave.save(function(err) {
		if (err) {callback(err);}
		modelsArray[obj].findOne({_id:documento._id}, function(err, doc) {
			if (err) {callback(err);}
			callback(null, doc);
		});
	});// toSave.save
}// salvarUm(contrutor, obj)

exports.salvarMuitos =  function (construtor, array, callback) {
	var obj = selectModel(construtor);
	var returnArray = [];
	modelsArray[obj].create(array, function() {
		if (arguments[0]) {callback(arguments[0]);}
		for (var i = 1; i < arguments.length; i++) {
			returnArray.push(arguments[i]);
		}
		callback(null, returnArray);
	});
}// salvarMuitos(contrutor, array)

exports.buscarUm = function (construtor, campo, termo, callback) {
	var obj = selectModel(construtor);
	modelsArray[obj].where(campo).equals(termo).exec(function(err, docs) {
		if (err) {callback(err);}
	 	callback(null, docs);
	});
}// buscarUm(construtor, campo, termo)

exports.buscarUmPopulate = function (construtor, campo, termo, populate, callback) {
	var obj = selectModel(construtor);

	modelsArray[obj].where(campo).equals(termo).exec(function(err, docs) {
		if (err) {callback(err);}
		var toPopulate = [];
		populate.forEach(function(elem) {
			toPopulate.push({path: elem.toString()});
		});
		modelsArray[obj].findOne({_id:docs[0]._id}).populate("cardapios").exec(function(err, docs) {
			if (err) {throw err;}
			callback(null, docs);
		});


		/*modelsArray[obj].populate(docs, toPopulate, function(err, docs) {
			if (err) {callback(err);}
			callback(null, docs);
		});*/



	});
}// buscarUmPopulate(construtor, campo, termo, populate)

exports.buscarMuitos = function (construtor, campo, array, callback) {
	var obj = selectModel(construtor);
	modelsArray[obj].where(campo).equals({$in:array}).exec(function(err, docs) {
		if (err) {callback(err);}
		callback(null, docs);
	});
}// buscarMuitos(construtor, campo, array)

exports.buscarUltimoDocumentoSalvo = function(construtor, callback) {
	var obj = selectModel(construtor);
	modelsArray[obj].findOne({}, {}, { sort: { 'created_at' : -1 } }, function(err, doc) {
  	if (err) {callback(err);};
  	callback(null, doc);
});
}

function selectModel (model) {
	for (var i = 0; i < modelsArray.length; i++) {
		if (modelsArray[i].modelName === model) {
			return i;
		}
	};
	return -1;
}
