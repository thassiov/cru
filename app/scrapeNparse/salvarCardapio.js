var raiz = require('./raiz');
var dbManager = require('./dbManager');

// modelos
var cardapio_dia = require(raiz() + '/app/models/c_cardapio_dia');
var cardapio_semana = require(raiz() + '/app/models/c_cardapio_semana');

/**
 * Salva o cardápio no banco de dados
 * O módulo recebe o objeto 'cardapio' como argumento, compara as comidas 
 * contidas nele com as do array de objetos 'comidaArray', atribui os 
 * ObjectId's para que o objeto 'cardapio' fique em conformidade com o 
 * modelo 'c_cardapio_dia' e depois agrupa todos os ObjectId's de 'cardapio_dia'
 *  em um 'cardapio_semana'. Todos os objetos gerados (dia e semana) são salvos
 *  no banco de dados.
 * @param  {object}   cardapio    Objeto semelhante ao modelo 'c_cardapio_semana', 
 * devidamente populado
 * @param  {array}   comidaArray Array com objetos 'c_comida'
 * @param  {Function} callback    Retorna um erro durante o armazenamento dos 
 * dados ou o ObjectId do 'cardapio_semana' salvo com sucesso no banco de dados.
 * @author Thassio Victor <tvmcarvalho@gmail.com>
 */
module.exports = function(cardapio, comidaArray, callback) {
	/*	* salvarCardapio (cardapio, comidaArray)
		"cardapio" é o objeto gerado por docParse
		"comidaArray" é o conjunto de objetos "comida" gerado pelo "registrarComida"
		* "arquivo" é a path para o arquivo salvo
	*/

	var arrayDeCardapios = [];
	var cardapioLoopControll = 0;

	var cardapioDaSemana = new cardapio_semana();
	cardapioDaSemana.registrado_em = cardapio.registrado_em;
	cardapioDaSemana.registrado_em_str = cardapio.registrado_em_str;
	cardapioDaSemana.vigencia_do_cardapio = cardapio.vigencia_do_cardapio;
	cardapioDaSemana.vigencia_do_cardapio_str = cardapio.vigencia_do_cardapio_str;
	cardapioDaSemana.cardapios = [];

	cardapio.cardapio.forEach(function(elem) {
		dbManager.buscarUm("cardapio_dia", "data_do_cardapio", elem.data_do_cardapio, function(err, doc) {
			if (err) {callback(err);}
			if (doc.length > 0) {
				if (arraysEqual(elem.almoco, doc[0].almoco) &&
					arraysEqual(elem.jantar, doc[0].jantar)) { 
					//Cardápio existe. Somente enviar ObjectId do cardápio
					cardapioDaSemana.cardapios.push(doc[0]._id);
					cardapioLoopControll++;
				} else {
					//Cardápio existe, mas informações divergem. Atualizar cardápio
					gerarCardapioDia(elem);
					cardapioLoopControll++;
				}
			} else {
				//Cardápio não existe. Gerar novo
				gerarCardapioDia(elem);
				cardapioLoopControll++;
			}	
			
			if (cardapio.cardapio.length == cardapioLoopControll) {
				dbManager.buscarUm("cardapio_semana", "vigencia_do_cardapio", 
					cardapioDaSemana.vigencia_do_cardapio, function(err, doc){
						if (err) {callback(err);}
						if (doc.length > 0) {
							var currentVig = cardapioDaSemana.vigencia_do_cardapio;
							var savedVig = doc[0].vigencia_do_cardapio;
							if (currentVig[0] == savedVig[0] &&
								currentVig[1] == savedVig[1]) {
								var currCardapioList = cardapioDaSemana.cardapios;
								var savedCardapioList = doc[0].cardapios;
								if (comparaOsArrayTudo(currCardapioList, savedCardapioList)) {
									console.log("Cardápio existente");
									callback(null, false);
								} else {
									salvarCardapios();
								}
							} else {
								salvarCardapios();
							}
						} else {
								salvarCardapios();
							}
					});
			}// if

		});
	}); // cardapio.cardapio.forEach

	/**
	 * Salva os objetos "cardapio_dia" e "cardapio_semana"
	 * @author Thassio Victor <tvmcarvalho@gmail.com>
	 */
	function salvarCardapios () {
		dbManager.salvarMuitos("cardapio_dia", arrayDeCardapios, function(err, docs) {
			if (err) {
				callback(err);
			}
			dbManager.salvarUm("cardapio_semana", cardapioDaSemana, function(err, doc) {
				if (err) {
					callback(err);
				}
				console.log("Cardapio da semana salvo!");
				callback(null, doc._id); 
			});
		});
	}

	/**
	 * Intancia e popula um objeto cardapio_dia
	 * @param  {Object} elem Objeto de onde serão extraídas as informações
	 * @author Thassio Victor <tvmcarvalho@gmail.com>
	 */
	function gerarCardapioDia (elem) {
		var cardapioDoDia = new cardapio_dia({
			registrado_em: elem.registrado_em,
			registrado_em_str: elem.registrado_em_str,
			dia_da_semana: elem.dia_da_semana,
			data_do_cardapio: elem.data_do_cardapio,
			data_do_cardapio_str: elem.data_do_cardapio_str,
			almoco: [],
			jantar: []
		});
		
		elem.almoco.forEach(function(almocoItem) {
			for (var i = 0; i < comidaArray.length; i++) {
				if(comidaArray[i].nome === almocoItem.nome){
					cardapioDoDia.almoco.push(comidaArray[i]._id);
					continue;
				}
			}
		}); // elem.almoco.forEach
		
		elem.jantar.forEach(function(jantarItem) {
			for (var i = 0; i < comidaArray.length; i++) {
				if(comidaArray[i].nome == jantarItem.nome){
					cardapioDoDia.jantar.push(comidaArray[i]._id);
					continue;
				}
			}
		}); // elem.jantar.forEach

		cardapioDaSemana.cardapios.push(cardapioDoDia._id);
		arrayDeCardapios.push(cardapioDoDia);
	}

	/**
	 * Verifica se dois arrays são iguais
	 * @param  {Array} arr1 
	 * @param  {Array} arr2 
	 * @return {Boolean}      
	 * Retirado de http://stackoverflow.com/a/4025958
	 */
	function arraysEqual(arr1, arr2) {
		
		if(arr1.length !== arr2.length)
			return false;

		var counter = 0;
		arr1.forEach(function(comida) {
			for (var i = 0; i < comidaArray.length; i++) {
				if(comidaArray[i].nome === comida.nome){
					if(comidaArray[i]._id !== arr2[counter])
						return false;
				}
			}
			counter++;
		});

		return true;
	}

	/**
	 * O mesmo que o de cima, só pra que outra parada
	 * To sem saco pra documentar esses bagulhos
	 */
	function comparaOsArrayTudo (array1, array2) {
	    if(array1.length !== array2.length)
	        return false;
	    
	    array1.forEach(function(ObjectId) {
	    	if (array2.indexOf(ObjectId == -1)) {
	    		return false;
	    	}
	    });
	    return true;
	}
};