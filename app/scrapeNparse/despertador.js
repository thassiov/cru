var dredd = require('./dredd');

/**
 * Módulo que inicia um contador temporal e dita a frequência de atualização 
 * do banco de dados do serviço.
 * @param  {object} app Intância do objeto 'express' declarado em server.js
 * @author Thassio Victor <tvmcarvalho@gmail.com>
 */
module.exports = function(app) {
	dredd();
	setInterval(function() {
		dredd();
	}, 1800000); // É feita uma nova checagem a cada 30 minutos

}