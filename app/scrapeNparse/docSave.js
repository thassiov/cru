// Módulos
var raiz = require('./raiz');
var dbManager = require('./dbManager');
var salvarComida = require('./salvarComida');
var salvarCardapio = require('./salvarCardapio');

/**
 * Salva o resultado do parsing de docParse no banco de dados
 * @param  {object}   cardapio      Objeto cardápio devidamente populado 
 * que será salvo no banco de dados.
 * @param  {array}   comidas2check Array de objetos 'c_comida' que serão 
 * salvos no banco de dados.
 * @param  {Function} callback      Erro durante a operação de armazenamento 
 * dos dados ou o ObjectId do documento 'c_cardapio_semana' que foi salvo.
 * @author Thassio Victor <tvmcarvalho@gmail.com>
 */
module.exports = function(cardapio, comidas2check, callback) {
	
	salvarComida(comidas2check, function(err, comidasArray) {
		if (err) {throw err;};
		salvarCardapio(cardapio, comidasArray, function(err, docId) {
			if (err) {throw err;};
			callback(null, docId);
		});
	});
}