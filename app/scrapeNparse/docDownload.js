var fs = require('fs');
var raiz = require('./raiz');
var request = require('request');
var replay = require('request-replay');
var moment = require('moment-timezone');

/**
 * Faz o download do index de ru.ufpa.br, salva o documento HTML resultante 
 * em um arquivo com nome padrão descrito na variável "output" e envia a PATH 
 * desse arquivo salvo por callback.
 * @param  {Function} callback Retorna um objeto que indica um erro durante o 
 * processo de download ou a PATH para o arquivo salvo.
 * @author Thassio Victor <tvmcarvalho@gmail.com>
 */
module.exports = function(callback) {
	var agora = moment().tz('America/Belem').
							format('DD MM YYYY HH mm ss').split(' ');

	var DD = agora[0],
			MM = agora[1],
			YYYY = agora[2],
			hh = agora[3],
			mm = agora[4],
			ss = agora[5];

	var URL = "http://ru.ufpa.br";
	var output = raiz() + "/app/ru-wget/ru_ufpa_br-"+YYYY+"_"+MM+
				"_"+DD+"-"+hh+"_"+mm+"_"+ss+".html";

	console.log("Iniciando download de " + URL);
	console.log("O destino do download será " + 
				raiz() + "/app/ru-wget/");

	var options = {
			url: URL,
			headers: {
				'User-Agent':'Mozilla/5.0 (X11; Linux x86_64; rv:38.0) Gecko/20100101 Firefox/38.0'
			},
			timeout: 15000, // 10 segundos
		};

replay(
	request(options, function(err, response, body) {
	 		if (!err && response.statusCode == 200) {
	 			tryAgain = true;
		 		fs.writeFile(output, body, function(err) {
		 			if (err) {
						callback(err);
					}
		 			else{
		 				console.log("Escrevendo no arquivo " + output);
		 				callback(null, output);
		 			}
		 		});
	 		}
	}),{
		retries: 5, 
		factor: 2
		}).on('replay', function(replay) {
			console.log('');
			console.log('### ERRO ###');
			console.log('URL: ' + options.url);
			console.log('Request de '+URL+' falhou: ' + replay.error.code + ' ' + replay.error.message);
    	console.log('Replay nr: #' + replay.number + ' de ' + URL);
  		console.log('Próxima tentativa de '+ URL +' em: ' + replay.delay + 'ms');
  		console.log('');
		});
};