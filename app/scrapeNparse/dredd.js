var moment = require('moment-timezone');
var docParse = require('./docParse');
var dbManager = require('./dbManager');
var docDownload = require('./docDownload');
var docSave = require('./docSave');
var raiz = require('./raiz');

// modelos
var execucao = require(raiz() + '/app/models/c_execucao');

/**
 * Inicia todo o processo de download, parsing e armazenamento dos dados 
 * retirados do site do RU.
 * @author Thassio Victor <tvmcarvalho@gmail.com>
 */
module.exports = function() {

	var exec = new execucao();
	exec.hora_inicio = moment().tz('America/Belem').format();
	docDownload(function(err, file) {
		if (err) {
			throw err;
		}
		exec.arquivo_gerado = file;
		docParse(file, function(err, cardapio, comidas2check) {
			if (err) {
				throw err;
			}
			docSave(cardapio, comidas2check, function(err, response){
				if(err){
					throw err;
				}
				if (response !== false) {
					exec.cardapio_gerado = response.toString();
				} else {
					exec.cardapio_gerado = "Não gerou cardapio";
				}
				exec.hora_termino = moment().tz('America/Belem').format();
				dbManager.salvarUm("execucao", exec, function(err, doc) {
					if (err) {throw err;}
					console.log("exec: " + doc);
				});				
			}); //registarComida
		}); //docParse
	}); //docDownload
};