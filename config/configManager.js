/**
 * configManager.js
 * Gerencia o arquivo de configuração do sistema.
 *
 * A tarefa desse módulo é buscar o arquivo de configuração para enviar 
 * seus dados para server.js.
 * Caso o arquivo de configuração não seja encontrado, um novo arquivo 
 * é criado e, por sua vez, lido e seus dados carregados por server.js.
 *
 * @author Thassio Victor <tvmcarvalho@gmail.com>
 */

var fs = require('fs');
var Q = require('q');
var deferred = Q.defer();

var createConfigFile = require('./createConfigFile');

var readFile = Q.denodeify(fs.readFile);

/**
 * Testa a existência da variável de ambiente OPENSHIFT_DATA_DIR
 * Caso ela não exista, o código não está sendo executado no OpenShift, 
 * então a path do arquivo de configuração é mudada para HOME.
 */
var configFilePath;
if (process.env.OPENSHIFT_DATA_DIR === undefined) {
 	configFilePath = process.env.HOME + '/.cru.conf';
} else {
	configFilePath = process.env.OPENSHIFT_DATA_DIR + '/.cru.conf';
}

module.exports = function (callback) {

	readFile(configFilePath, 'utf-8').
		then(function readSuccessful (value) {
			deferred.resolve(value);
		}).
		fail(function configFileFail (reason) {
			//@TODO: verificar motivo da falha na leitura
			//@TODO: iniciar criador de arquivo de configuração
			createConfigFile(configFilePath).
				then(function configFileCreationSuccessful (value) {
					deferred.resolve(value);
				}).
				fail(function configFileCreationFailed (reason) {
					deferred.reject(reason);
				});
		});

	// callback support
	// callback(error,data)
	deferred.promise.nodeify(callback);
	return deferred.promise;
};