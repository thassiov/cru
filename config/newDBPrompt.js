/**
 * newDBPrompt.js
 * Armazena informações do usuário sobre o banco de dados
 *
 * O objeto resultante do questionário é o seguinte:
  * {
 * 		dbTitle: 'Titulo do banco', // Um nome para identificar o banco
 * 		dbUrl: 'url.do.banco.de.dados.com', // Pode ser 'localhost' sem problema
 * 		dbPort: 27017, // Porta de acesso ao banco
 * 		dbName: 'nome_do_banco',
 * 		dbUser: 'nome_do_usuario',
 * 		dbPassword: 'password_do_usuario'
 * 	}
 *
 * @author Thassio Victor <tvmcarvalho@gmail.com>
 */

var Q = require('q');
var deferred = Q.defer();

var prompt = require('prompt');
var promptGet = Q.denodeify(prompt.get);

module.exports = function (callback) {

/**
 * Questionário usado pelo prompt
 * @type {Object}
 */
var promptOpts = {
	properties: {
		dbUrl: {
			description: 'Defina a URL do banco de dados',
			default: 'localhost'
		},
		dbPort: {
			description: 'Defina a porta do banco de dados',
			default: 27017
		},
		dbName: {
			description: 'Defina o nome do banco de dados',
			required: true,
			message: 'Você precisa definir um nome válido'
		},
		dbUser: {
			description: 'Defina o nome do usuário do banco de dados',
			required: true,
			message: 'Você precisa definir o nome do usuário para ter acesso ao banco de dados'
		},
		dbPassword: {
			description: 'Defina a senha do usuário do banco de dados',
			default: ''
		},
		dbTitle : {
			description: 'Nome para essa configuração (ex: \'Banco remoto\')',
			default: ''
		}
	}
};

promptGet(promptOpts).
	then(function handlePromptResponse(value) {
		deferred.resolve(value);
	}).
	fail(function failed (reason) {
		deferred.reject(reason);
	});

	// callback support
	// callback(error, data)
	deferred.promise.nodeify(callback);
	return deferred.promise;
};