/**
 * createConfigFile.js
 * Cria o arquivo de configuração que o sistema irá usar
 *
 * Quando executado, um pequeno questionário é apresentado ao usário para 
 * adicionar informações sobre o banco de dados e o usuário do mesmo. 
 * Depois de respondidas as perguntas, o arquivo de configuração é escrito.
 *
 * O arquivo com as configurações tem o nome .cru.conf e é escrito na home 
 * do usuário que iniciou o processo (path definida por 'process.env.HOME').
 *
 * A estrutura do arquivo de configuração é a seguinte:
 *
 * {
 * 	database: [
 * 		{
 * 		dbTitle: 'Titulo do banco', // Um nome para identificar o banco
 * 		dbUrl: 'url.do.banco.de.dados.com', // Pode ser 'localhost' sem problema
 * 		dbPort: 27017, // Porta de acesso ao banco
 * 		dbName: 'nome_do_banco',
 * 		dbUser: 'nome_do_usuario',
 * 		dbPassword: 'password_do_usuario'
 * 		}
 * 	]
 * }
 *
 * @author Thassio Victor <tvmcarvalho@gmail.com>
 */

var Q = require('q');
var deferred = Q.defer();

var fs = require('fs');
var readFile = Q.denodeify(fs.readFile);
var fsWriteFile = Q.denodeify(fs.writeFile);

var newDBPrompt = require('./newDBPrompt');

module.exports = function (configFilePath, callback) {

	newDBPrompt().
		then(function handlePromptResponse(value) {
			var configFileContent = {};
			
			configFileContent.database = [];
			configFileContent.database.push(value);
			Q.nfcall(fs.writeFile, 
					configFilePath,
					JSON.stringify(configFileContent, null, 2)
					);

			return Q.nfcall(fs.readFile, configFilePath, 'utf8');
		}).
		then(function finallySendConfigs (value) {
			deferred.resolve(value);
		}).
		fail(function failed (reason) {
			deferred.reject(reason)					;
		});

	// callback support
	// callback(error, data) 
	deferred.promise.nodeify(callback);
	return deferred.promise;

};