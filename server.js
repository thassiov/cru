var Q = require('q'),
		fs = require('fs'),
		morgan = require('morgan'),
		express = require('express'),
		mongoose = require('mongoose'),	
		favicon = require('serve-favicon'),
		configs = require('./config/configManager'),
		routeManager = require('./app/router/routeManager');

	configs().
		then(function result (value) {
			var configObj = JSON.parse(value);

			//@TODO: criar função para seleção de banco de dados baseado nas infos 
			//				do cru.conf
			var mongoURL = 'mongodb://' + 
											configObj.database[0].dbUser + ':' +
											configObj.database[0].dbPassword + '@' +
											configObj.database[0].dbUrl + ':' + 
											configObj.database[0].dbPort + '/' +
											configObj.database[0].dbName;

			console.log('Conectando no banco \'' + configObj.database[0].dbTitle + '\'');
			console.log('Endereço do banco: ' + configObj.database[0].dbUrl);
			console.log('Usuário: ' + configObj.database[0].dbUser);
			mongoose.connect(mongoURL);

			//@TODO: Trocar esses parâmetros no openshift
			var port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
			var ipaddr = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';

			var app = express();

			// LOGS
			var accessLogStream = fs.createWriteStream('./logs/access.log', {flags: 'a'});
			app.use(morgan('combined', {stream: accessLogStream}));

			app.use(express.static(__dirname + '/public'));

			//@TODO: refazer todo esse módulo 'scrapeNparse' (branch cru-scraping)
			// require('./app/scrapeNparse/despertador')(app);
			
			//@NOTE: trabalhando nas rotas
			routeManager(app);

			app.listen(port, ipaddr);
		}).
		fail(function failed (reason) {
			//@TODO: melhorar mensagem para entrar nos logs
			console.error(reason);
		});

